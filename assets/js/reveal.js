
        
    // ScrollReveal().reveal('header .container', {
    //     duration: 1000,
    //     reset: false,
    //     distance: '200px',
    //     origin: 'top'
    // });
    // ScrollReveal().reveal('.main-banner .one-side-container', {
    //     delay: 100,
    //     duration: 1000,
    //     reset: false,
    //     distance: '500px',
    //     origin: 'right'
    // });
    // ScrollReveal().reveal('.main-banner .one-side-container-outside', {
    //     delay: 400,
    //     duration: 1000,
    //     reset: false,
    //     distance: '200px',
    //     origin: 'left'
    // });
    ScrollReveal().reveal('.service-block h2.title', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.text-block h2.title', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.news-block h2.title', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.partners-block h2.title', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.text-block .bg-gray', {
        delay: 300,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'left',
        viewOffset: {
            top: 0,
            bottom: 0,
        },
    });
    ScrollReveal().reveal('.text-block .image-half', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'right',
        viewOffset: {
            top: 0,
            bottom: 0,
        },
    });
    ScrollReveal().reveal('.service-block .flexy a:nth-child(4n-1)', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.service-block .flexy a:nth-child(4n-2)', {
        delay: 300,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.service-block .flexy a:nth-child(4n-3)', {
        delay: 200,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.service-block .flexy a:nth-child(4n)', {
        delay: 400,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.count-banner .one-side-container', {
        delay: 100,
        duration: 1000,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.count-banner .one-side-container-outside', {
        delay: 600,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'right'
    });
    ScrollReveal().reveal('.count-banner .containered.halfies .num-card:nth-child(4n - 3)', {
        delay: 600,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.count-banner .containered.halfies .num-card:nth-child(4n - 2)', {
        delay: 700,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.count-banner .containered.halfies .num-card:nth-child(4n - 1)', {
        delay: 500,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.count-banner .containered.halfies .num-card:nth-child(4n)', {
        delay: 800,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.projects-block:not(.projects-page) .one-project:nth-child(4n - 3)', {
        delay: 400,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.projects-block:not(.projects-page) .one-project:nth-child(4n - 2)', {
        delay: 200,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.projects-block:not(.projects-page) .one-project:nth-child(4n - 1)', {
        delay: 300,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.projects-block:not(.projects-page) .one-project:nth-child(4n)', {
        delay: 100,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'bottom'
    });
    ScrollReveal().reveal('.news-block .news-card:nth-child(2n - 1)', {
        delay: 0,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.news-block .news-card:nth-child(2n)', {
        delay: 200,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'right'
    });
    ScrollReveal().reveal('.partners-block .text-half-block', {
        delay: 300,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.partners-block .images-block', {
        delay: 700,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'right'
    });

    ScrollReveal().reveal('.subscription-block .text-half-block', {
        delay: 500,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'left'
    });
    ScrollReveal().reveal('.subscription-block .input-block', {
        delay: 50,
        duration: 600,
        reset: false,
        distance: '500px',
        origin: 'right'
    });

