$( document ).ready(function() {

    
    // Language change animation ---------------
    $('.lang-change .button').click(function(){
        $(this).toggleClass('en');

        // Language change Redirect -----------

        setTimeout( function() {
            $(location).attr('href', $('.lang-change .button').data('link'));
        }, 500);

    })


    // Post Register form---------------

    $('.reveal-btn').click(function(){
        $(this).parent().toggleClass('open');
    })


    // Multiple File Upload----------------------


    $(".add-file input").on('change', function (e) {
        var files = this.files;
        if (!files.length) {
          // $(this).prev().find(".form-control .label:hidden").show();
          // $(this).prev().find(".form-control .file-list").html('');
          return;
        }
        // $(this).prev().find(".form-control .label").hide();
        var fileNames = [];
        for (var i = 0; i < files.length; i++) {
          fileNames.push(files[i].name);
        }
        var text = fileNames.join(", ");
        $(this).parent().find('label').html('<span class="icon-attachment-clip"></span> '+text);
      });


      $(document).ready(function () {
        setTimeout(function() {
          
          // Handler for .ready() called.
          $('html, body').animate({
              scrollTop: $('.scroll-to').offset().top - 60
          }, 'slow');

          
        }, 400);
      });
      

});

